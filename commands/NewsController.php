<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;


use app\models\Content;
use app\models\ParseAllNews;
use yii\console\Controller;



/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class NewsController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
//    public function actionIndex($message = 'hello world')
//    {
//        echo $message . "\n";
//    }
    private $site_url;
    public function actionNews()
    {
        $arr = ParseAllNews::newsList();
        $i = 0;
        foreach ($arr as $key => $value){
            $model = new ParseAllNews();
            $model->img = $arr[$key]["img"];
            $model->text = $arr[$key]["text"];
            $model->title = $arr[$key]["title"];
            $model->url = $arr[$key]["site_url"];
            $i++;
            $this->site_url[$i] = $arr[$key]["site_url"];
            $model->save();
        }
        $this->Parsernews();
    }

    public function Parsernews()
    {
        $site_url = $this->site_url;
        foreach ($site_url as $val){
            $news = Content::OneNews($val);
            foreach ($news as $key => $value){
                $model = new  Content();
                $model->img = $news[$key]["img"];
                $model->text = $news[$key]["text"];
                $model->title = $news[$key]["title"];
                $model->text_preview = $news[$key]["text_preview"];
                $model->save();
            }
        }
    }





}
