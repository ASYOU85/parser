<?php

namespace app\models;
use GuzzleHttp\Client;
use Yii;
use yii\helpers\BaseStringHelper;

/**
 * This is the model class for table "content".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * * @property string $text_preview
 * @property string $img
 * @property integer $news_id
 *
 * @property News $news
 */
class Content extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text',], 'required'],
            [['text', 'text_preview'], 'string'],
            [['text_preview'], 'string', 'max' => 150],
            [['title', 'img'], 'string', 'max' => 255],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'text' => 'Текст',
            'img' => 'Картинка',
            'text_preview' => 'Анонс',
        ];
    }
    public static function getAll(){
        $data = self::find();
        return $data;
    }

    public static function OneNews($site_url){

        // создаем экземпляр класса
        $client = new Client();
        // отправляем запрос к странице Яндекса
        $res = $client->request('GET', $site_url);
        // получаем данные между открывающим и закрывающим тегами body
        $body = $res->getBody();
        // подключаем phpQuery
        $document = \phpQuery::newDocumentHTML($body);
        // получаем список новостей
        $news = $document->find('.b-topic');
        // выполняем проход циклом по списку
        foreach ($news as $article) {
            //pq аналог $ в jQuery
            $article = pq($article);
            $img = $article->find('img')->attr('src');
            $text = $article->find('.b-text')->html();
            $title = $article->find('.b-topic__title')->html();
            $rightcol = $article->find('.b-topic__rightcol')->html();
            $text_preview = strip_tags(BaseStringHelper::truncate($text, 150, '...'));
            $i++;
            $mas[$i] = array('title' => $title, 'img' => $img, 'text' => $text, 'rightcol' => $rightcol,'text_preview' => $text_preview);

        }
        return $mas;
    }
}
