<?php

namespace app\models;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $img
 * @property string $url
 *
 * @property Content[] $contents
 */

class News extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text', 'url'], 'required'],
            [['text'], 'string'],
            [['title', 'url'], 'string', 'max' => 255],
        ];
    }

    public static function tableName(){
        return 'news';
    }

   public static function newsList(){
        $data = self::find()->all();
       return $data;
    }

}