<?php

namespace app\models;
use GuzzleHttp\Client;
use yii\db\ActiveRecord;


class ParseAllNews extends ActiveRecord
{

    public static function tableName()
    {
        return 'news';
    }

    public function  attributeLabels()
    {
        return [
                    'id' => 'ID',
                    'img' => 'Картинка',
                    'text' => 'Текст',
                    'title' => 'Заглавие',
                    'url' => 'Ссылка'

                ];
    }

    public function rules()
    {
        return [
                [['text', 'title', 'url'], 'required'],
                [['img', 'text', 'title', 'url'], 'trim']
                ];
    }

    public static function newsList(){
        $site = "http://lenta.ru/";

        $client = new Client();

        $res = $client->request('GET', 'http://lenta.ru');

        $body = $res->getBody();

        $document = \phpQuery::newDocumentHTML($body);

        $links = $document->find('.bordered-title');

        foreach ($links as $link) {
            $link = pq($link);
            $a = $link->find('a', 0)->attr('href');
            $site_url = $site . $a;
            $get = $client->request('GET', $site_url);
            $body_get = $get->getBody();
            $document = \phpQuery::newDocumentHTML($body_get);
            $links_get = $document->find('.b-longgrid-column .item.article');
            foreach ($links_get as $link_get){
                $url = pq($link_get)->find('.titles h3 a')->attr('href');
                if (strpos($url, '://') !== FALSE) {
                   continue;
                } else{
                    $site_url = $site . $url;
                }
                if(!News::find()->where( [ 'url' => $site_url ] )->exists())
                {
                    $img = pq($link_get)->find('img')->attr('src');
                    $text = pq($link_get)->find('.rightcol')->html();
                    $title = pq($link_get)->find('.titles a>span')->html();
                }
                $i++;
                $mas[$i] = array('title' => $title, 'img' => $img, 'text' => $text, 'site_url' => $site_url, );

            }
        }
        return $mas;
    }
}

