<?php
/**
 * Created by PhpStorm.
 * User: userpr
 * Date: 20.10.2016
 * Time: 18:34
 */

namespace app\models;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "main_news".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $img
 * @property string $url
 */
class ViewMainNews extends ActiveRecord
{
    public static function tableName(){
        return 'main_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text', 'url'], 'required'],
            [['title', 'img', 'url'], 'string', 'max' => 255],
            [['text'], 'string', 'max' => 130],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'text' => 'Text',
            'img' => 'Img',
            'url' => 'Url',
        ];
    }

    public static function getAll(){
        $data = self::find()->limit(6)->all();
        return $data;
    }
    public static function getSideBar(){
        $data = self::find()->limit(4)->all();
        return $data;
    }
}