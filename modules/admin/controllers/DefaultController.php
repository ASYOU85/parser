<?php

namespace app\modules\admin\controllers;

use app\models\LoginForm;
use Yii;
use app\modules\admin\models\MainNews;
use app\modules\admin\models\MainNewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DefaultController implements the CRUD actions for MainNews model.
 */
class DefaultController extends Controller
{
    private  $wmax = 300;
    private $hmax = 200;

    public function getWmax() {
        return $this->wmax;
    }

    public function getHmax() {
        return $this->hmax;
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MainNews models.
     * @return mixed
     */
    public function actionIndex()
    {
        {
            $searchModel = new MainNewsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single MainNews model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    function transliterate($input){
        $gost = array(
            "Є"=>"YE","І"=>"I","Ѓ"=>"G","і"=>"i","№"=>"-","є"=>"ye","ѓ"=>"g",
            "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
            "Е"=>"E","Ё"=>"YO","Ж"=>"ZH",
            "З"=>"Z","И"=>"I","Й"=>"J","К"=>"K","Л"=>"L",
            "М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R",
            "С"=>"S","Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"X",
            "Ц"=>"C","Ч"=>"CH","Ш"=>"SH","Щ"=>"SHH","Ъ"=>"'",
            "Ы"=>"Y","Ь"=>"","Э"=>"E","Ю"=>"YU","Я"=>"YA",
            "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
            "е"=>"e","ё"=>"yo","ж"=>"zh",
            "з"=>"z","и"=>"i","й"=>"j","к"=>"k","л"=>"l",
            "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
            "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"x",
            "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"shh","ъ"=>"",
            "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
            " "=>"_","—"=>"_",","=>"_","!"=>"_","@"=>"_",
            "#"=>"-","$"=>"","%"=>"","^"=>"","&"=>"","*"=>"",
            "("=>"",")"=>"","+"=>"","="=>"",";"=>"",":"=>"",
            "'"=>"","~"=>"","`"=>"","?"=>"","/"=>"",
        );

        return strtr($input, $gost);
    }

    /**
     * Creates a new MainNews model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {


        $model = new MainNews();

        if ($model->load(Yii::$app->request->post())) {

            //****Ресайз***
//            $dir = Yii::getAlias('images/');
//            $model->img = UploadedFile::getInstance($model, 'img');
//            $fileName = $model->img->baseName . '.' . $model->img->extension;
//            $model->img->saveAs('images/'. $model->img->baseName.'.'.$model->img->extension);
//            $model->img = 'images/'. $model->img->baseName.'.'.$model->img->extension;
//            $model->save();
//            $model->img = $fileName; // без этого ошибка
//// Для ресайза фотки до 800x800px по большей стороне надо обращаться к функции Box() или widen, так как в обертках доступны только 5 простых функций: crop, frame, getImagine, setImagine, text, thumbnail, watermark
//            $photo = Image::getImagine()->open($dir . $fileName);
//            $photo->thumbnail(new Box(200, 300))->save($dir . $fileName, ['quality' => 100]);
            //****Ресайз***

            //если хотим чтобы название файла совпадало с title зменитиь строки ниже на:
//            $imgName = $model->title;
//            $model->img = UploadedFile::getInstance($model, 'img');
//            $model->img->saveAs('images/'. $imgName.'.'.$model->img->extension);
//            $model->img = 'images/'. $imgName.'.'.$model->img->extension;


            $model->img = UploadedFile::getInstance($model, 'img');

            $model->img->saveAs('images/'. $this->transliterate($model->title).'.'.$model->img->extension);
            $model->img = 'images/'. $this->transliterate($model->title).'.'.$model->img->extension;
            list($w_orig, $h_orig) = getimagesize($model->img);
            if( $this->wmax == $w_orig AND $this->hmax == $h_orig ) {
                $model->save();
            }else{
                echo "Размер картинки должен быть высота: 200 ширина: 300";
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MainNews model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->img = UploadedFile::getInstance($model, 'img');
            $model->img->saveAs('images/'. $this->transliterate($model->title).'.'.$model->img->extension);
            $model->img = 'images/'. $this->transliterate($model->title).'.'.$model->img->extension;
            list($w_orig, $h_orig) = getimagesize($model->img);
            if( $this->wmax == $w_orig AND $this->hmax == $h_orig ) {
                $model->save();
            }else{
                echo "Размер картинки должен быть высота: 200 ширина: 300";
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MainNews model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MainNews model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MainNews the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MainNews::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
