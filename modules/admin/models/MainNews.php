<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "main_news".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $url
 */
class MainNews extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'main_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text', 'img', 'url'], 'required'],
            [['text'], 'string'],
//            [['img'], 'file'],
            [['title', 'url'], 'string', 'max' => 255],
            [['img'], 'file', 'extensions' => 'png, jpg, bmp, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заглавие',
            'text' => 'Текст',
            'img' => 'Картинка',
            'url' => 'Ссылка на новость',
        ];
    }
}
