<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\MainNews */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="main-news-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->widget(CKEditor::className(), [
        'options' => ['rows' => 3],
        'preset' => 'basic'
    ], ['maxlength' => true])->hint('Максимальная длинна 130 знаков') ?>

    <?= $form->field($model, 'img')->fileInput()->hint('Размер картинки должен быть высота: 200 ширина: 300') ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true])->hint('Пример: http://examle.com')?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
