<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\MainNews */

$this->title = 'Create Main News';
$this->params['breadcrumbs'][] = ['label' => 'Main News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-news-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
