<?php

use \yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contents';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-index">

    <h1>Главные новости</h1>
    <!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <!---->
    <!--    <p>-->
    <!--        --><? //= Html::a('Create Content', ['create'], ['class' => 'btn btn-success']) ?>
    <!--    </p>-->

    <div class="row">

        <?php foreach ($array as $news) { ?>
            <div class='col-sm-6 col-md-4'>
                <div class='thumbnail programm'>
                    <img src=<?= $news->img ?> alt='<?= $news->title ?>'>
                    <div class='caption'>
                        <h3><?= $news->title ?></h3>
                        <div class="caption_text">
                            <p class="text"><?= strip_tags($news->text) ?></p>
                        </div>
                        <p><a href="<?= $news->url ?>" class="btn btn-primary" role="button">Детальнее</a></p>
                    </div>
                </div>
            </div>
        <? } ?>
        <?php foreach ($models as $arr) { ?>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="picture">
                        <img src="<?= $arr->img ?>" height='200' width='300' alt="...">
                    </div>
                    <div class="caption">
                        <h3><?= $arr->title ?></h3>
                        <div class="caption_text">
                            <p class="text"><?= strip_tags($arr->text_preview) ?></p>
                        </div>
                        <p><a href="\parser\view?id=<?= $arr->id ?>" class="btn btn-primary" role="button">Детальнее</a>
                        </p>
                    </div>
                </div>
            </div>

        <?php } ?>
    </div>
</div>

<?php
echo LinkPager::widget([
    'pagination' => $pages,
]);
?>
