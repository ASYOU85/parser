<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\Content */

$this->title = $model->title;
//$this->params['breadcrumbs'][] = ['label' => 'Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-view">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    вывод новостей наших-->
    <div class="row">
        <div id="aside1" class='col-md-3'>
            <?php foreach ($array as $news) { ?>
                <a href="<?= $news->url ?>">
                    <div class='thumbnail programm'>
                        <img src=<?= $news->img ?> height='50' width='100' alt='images'>
                        <div class='caption'>
                            <h3><?= $news->title ?></h3>
                        </div>
                    </div>
                </a>
            <? } ?>
        </div>
        <!--    вывод новостей наших-->
        <!--    вывод сторонних новостей-->
        <div class='col-md-9'>
            <div class="panel panel-default">
                <!--        <div class="panel-heading">--><? //= $model->title ?><!--</div>-->
                <div class="panel-body"><?= $model->text ?></div>
                <p>Источник: <a href="http:\\www.lenta.ru">Lenta.ru</p>
            </div>
        </div>
        <!--    вывод сторонних новостей-->
    </div>
</div>

